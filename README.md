# a127docker
An a127 Docker instance wired for my needs

## Base install

The instance uses Vagrant as the provisioner.

* Centos7 w/ Puppet - puppetlabs/centos-7.0-64-puppet

# Modules required

* java
* maven
* Docker
* nodeJs
* git
* jenkins

# To update inside IAG network

>vagrant box add --insecure puppetlabs/centos-7.0-64-puppet

>vagrant up