include git

include java

Maven {
  user  => "maven", # you can make puppet run Maven as a specific user instead of root, useful to share Maven settings and local repository
  group => "maven", # you can make puppet run Maven as a specific group
  repos => "http://repo.maven.apache.org/maven2"
}

class { "maven::maven":
  version => "3.3.9", # version to install
  # you can get Maven tarball from a Maven repository instead than from Apache servers, optionally with a user/password
  repo => {
    #url => "http://repo.maven.apache.org/maven2",
    #username => "",
    #password => "",
  }
}


