#!/usr/bin/env bash

# Set timezone to Melbourne, Australia
timedatectl set-timezone Australia/Melbourne 

# Update
yum -y update

# Minimal install - support deltarpm's - issue with puppet modules
yum -y install deltarpm

echo "RUNNING the Bash"

# install puppet modules
puppet module install puppetlabs-git
puppet module install puppetlabs-java
#puppet module install puppetlabs-nodejs
puppet module install maestrodev-maven
#puppet module install puppetlabs-docker_platform
puppet module install rtyler-jenkins


# NodeJs - Puppet issues
# https://nodejs.org/en/download/package-manager/ - nodeJs install instr.

curl --silent --location https://rpm.nodesource.com/setup_4.x | bash -
yum -y install nodejs

# Install Docker (Puppet script issue on this vagrant box
curl -fsSL https://get.docker.com/ | sh

puppet apply -d /vagrant_data/puppetInstall.pp 

sudo service docker start &

